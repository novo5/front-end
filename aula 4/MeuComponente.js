import { Estilisada } from "./stilo";

const MeuComponente = () => {
    return (
        <Estilisada>
            <p>Texto do meu componente</p>
        </Estilisada>
    )
}

export default MeuComponente;
